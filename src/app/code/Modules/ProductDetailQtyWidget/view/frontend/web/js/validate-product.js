define(['jquery'], function ($) {
  'use strict';

  return function (addToCart) {
      $('#minusQty').click(function () {

        var qty = parseInt($('#qty').val());
        qty = qty - 1;
        $('#qty').val(qty).trigger('change');
      });

      $('#addQty').click(function () {

        var qty = parseInt($('#qty').val());
        qty = qty + 1;
        $('#qty').val(qty).trigger('change');
      });

      $('#qty').on('change', function() {
        var qty = parseInt($(this).val());
        if (qty > 100) {
            $(this).val('100');
        } else if (qty < 1) {
            $(this).val('1');
        }
    });

      return addToCart;
  }
});




