var config = {
  map: {
    '*': {
      'Magento_Checkout/template/minicart/item/default.html': 'Modules_Qty/template/minicart/item/default.html',
      'sidebar': 'Modules_Qty/js/sidebar',
      'Magento_Checkout/js/view/minicart': 'Modules_Qty/js/view/minicart'
    }
  }
};