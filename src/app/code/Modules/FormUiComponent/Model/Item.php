<?php namespace Modules\FormUiComponent\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Item
 * @package Modules\FormUiComponent\Model
 */
class Item extends AbstractModel
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(\Modules\FormUiComponent\Model\ResourceModel\Item::class);
    }
}