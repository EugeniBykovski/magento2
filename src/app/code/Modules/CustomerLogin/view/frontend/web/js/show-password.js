define([
	'jquery',
	'mage/mage',
	'Magento_Catalog/product/view/validation',
	'catalogAddToCart'
], function ($) {
	'use strict';

	$.widget('mage.logWidget', {
			options: {
					bindSubmit: false,
					radioCheckboxClosest: '.nested',
					addToCartButtonSelector: '.action.tocart'
			},

			/**
			 * Uses Magento's validation widget for the form object.
			 * @private
			 */
			_create: function () {
				var elem = this.element;
				var $btn = $('<a class="password-control"></a>');

				$btn.on('click', function(){
					var foo = $(this).prev().attr("type");
					if(foo == "password"){
							$(this).prev().attr("type", "text");
							$('.password-control').css("background", "url(https://snipp.ru/demo/495/no-view.svg) 0 0 no-repeat")
					} else {
							$(this).prev().attr("type", "password");
							$('.password-control').css("background", "url(https://snipp.ru/demo/495/view.svg) 0 0 no-repeat")
						}
				});

				$btn.insertAfter(elem);
			}
	});

	return $.mage.logWidget;
});


